function trainer(name, age, pokemon, friends){
	this.name = name;
	this.age = age;
	this.pokemon = pokemon;
	this.friends = friends;
	this.talk = function(){
		console.log("Pikachu I choose you!");
	};
}

let newTrainer = new trainer(
	"Ash Ketchum", 
	10,
	["Pikachu", "Charizard", "Squirtle", "Bulbasuar"],
	 {
        koenn: ["May", "Max"],
        kanto: ["Brock", "Misty"]
     }
	);
console.log(newTrainer);
console.log("Result of dot notation:");
console.log(newTrainer.name);
console.log("Result of square bracket notaion:");
console.log(newTrainer["pokemon"]);
console.log("Result of square talk method:");
newTrainer.talk();

function pokemon(name, level, health, attack){
	this.name = name;
	this.level = level;
	this.health = level * 3;
	this.attack = level * 1.5;
	this.tackle = function(targetPokemon){
		console.log(this.name + " tackled " + targetPokemon.name);
	};

	this.faint = function(){
		console.log(this.name + " has fainted.")
	};
}

let newPokemon = new pokemon("Pikachu", 12);
console.log(newPokemon);

let newPokemon2 = new pokemon("Geodude", 8);
console.log(newPokemon2);
let newPokemon3 = new pokemon("Mewtwo", 100);
console.log(newPokemon3);

newPokemon2.tackle(newPokemon);
newPokemon3.tackle(newPokemon2);
newPokemon3.faint();








